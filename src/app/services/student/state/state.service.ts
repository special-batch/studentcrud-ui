import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SelectItem } from 'primeng/api'
import { State } from '../../../model/state.model'
import { Observable } from 'rxjs';


@Injectable()
export class StateService {
  allState: SelectItem[] = [];
  mockUrl: string = "http://localhost:3000/state";

  constructor(
    private http: HttpClient
  ) { }

  getAllState(): Observable<State[]> {
    return this.http.get<State[]>(this.mockUrl);
  }
}
