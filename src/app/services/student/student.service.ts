import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { Student } from '../../model/student.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class StudentService {
  allStudent: Student[];

  mockUrl: string = "http://localhost:3000/student";
  
  currentStudent: Student = {
    name: "",
    branch: "",
    sport: "",
    address: "",
    prnNumber: null,
    code: "",
    id: null
  }

  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ) { }

  getAllStudent(): Subscription {
    this.spinner.show();
    return this.http.get<Student[]>(this.mockUrl).subscribe(
      (allStudent: Student[]) => {
        this.allStudent = allStudent;
        setTimeout(() => {
          this.spinner.hide();
        }, 1000);
      });
  }

  deleteStudent(studentId: number): Observable<Student> {
    return this.http.delete<Student>(this.mockUrl + '/' + studentId);
  }

  editStudent(currentStudent: Student) {
    return this.http.post(this.mockUrl, currentStudent);
  }

  createStudent(currentStudent: Student): Observable<Student> {
    return this.http.post<Student>(this.mockUrl, currentStudent);
  }

  updateStudent(student: Student): Observable<Student> {
    return this.http.put<Student>(this.mockUrl + '/' + this.currentStudent.id, this.currentStudent);
  }
}
