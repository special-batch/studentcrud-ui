export declare class State {
    id: number;
    name: string;
    code: string;
    state: string
}