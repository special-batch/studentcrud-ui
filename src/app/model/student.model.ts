export declare class Student {
    id: number;
    name: string;
    branch: string;
    prnNumber: number;
    address: string
    sport: string;
    code: string;
}