import { Component, OnInit, } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Student } from 'src/app/model/student.model';
import { StudentService } from './../../../services/student/student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  constructor(
    public studentService: StudentService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.getAllStudent();
  }

  getAllStudent() {
    this.studentService.getAllStudent();
  }

  deleteStudent(id: number) {
    this.studentService.deleteStudent(id).subscribe(
      (student: Student) => {
        this.getAllStudent();
        this.toastr.error("Student Deleted Successsfully", "Student-CRUD-ui")
      });
  }

  editStudent(student: Student) {
    console.log(student);
    this.studentService.currentStudent = Object.assign({}, student)
  }
}

