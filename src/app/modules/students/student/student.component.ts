import { Component, OnInit } from '@angular/core';
import { StudentService } from './../../../services/student/student.service';
import { Student } from '../../../model/student.model';
import { ToastrService } from 'ngx-toastr';
import { StateService } from './../../../services/student/state/state.service';
import { State } from '../../../model/state.model';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  allState: SelectItem[] = [];

  constructor(
    public studentService: StudentService,
    private toastr: ToastrService,
    private stateService: StateService
  ) { }

  ngOnInit(): void {
    this.getAllState();
  }

  save(student: Student) {
    if (student.id == null) {
      console.log('CREATE COMPANY');
      this.createStudent(student);
    } else {
      console.log('upadate Company');
      this.updateStudent(student);
    }
  }

  createStudent(student: Student) {
    this.studentService.createStudent(student).subscribe(
      (currentStudent: Student) => {
        this.studentService.getAllStudent();
        this.clear();
        this.toastr.success("Student Created successfully", "Student-CRUD")
      });
  }

  updateStudent(student: Student) {
    this.studentService.updateStudent(student).subscribe(
      (currentStudent: Student) => {
        this.studentService.getAllStudent();
        this.clear();
        this.toastr.info("student updated Successfully", "student-CRUD")
      });
  }

  clear() {
    this.studentService.currentStudent = {
      name: "",
      branch: "",
      sport: "",
      address: "",
      prnNumber: null,
      code: "",
      id: null
    }
  }

  getAllState() {
    this.stateService.getAllState().subscribe(
      (allState: State[]) => {
        allState.forEach(
          (state) => {
            this.allState.push({ label: state.name, value: state.code });
          });
      });
  }
}
