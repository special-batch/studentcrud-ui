import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentComponent } from './student/student.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentService } from './../../services/student/student.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown'
import { StateService } from './../../services/student/state/state.service';

@NgModule({
  declarations: [StudentComponent, StudentListComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    DropdownModule
  ],
  exports: [
    StudentComponent,
    StudentListComponent
  ],
  providers: [
    StudentService,
    StateService
  ]
})
export class StudentsModule { }
